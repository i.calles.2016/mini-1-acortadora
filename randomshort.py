import random
import webapp
import urllib.parse
import shelve

PAGE_URL_DICT = """
                <!DOCTYPE html>
                <html lang="en">
                <form action="/" method="post">
                        URL to shorten: <input type="text" name="url">
                        <input type="submit" value="Submit">
                    </form> 
                    <body>
                        <p>Shortened URLS History: </p>
                        {url_dict}
                    </body>
                </html>
                """


REDIRECT = '''
            <!DOCTYPE html>
            <html lang="es">
                <head>
                    <meta charset="UTF-8">
                    <meta http-equiv="refresh" content="3;url={key}">
                    <title>Redirección</title>
                </head>
                <body>
                    <h1>Redireccionando...</h1>
                    <p>Por favor, espera un momento mientras te redirigimos a <a href={key}>{key}</a>.</p>
                </body>
            </html> 
            '''

PAGE_RESOURCE_NOT_FOUND = '''
            <!DOCTYPE html>
            <html lang="es">
                <body>
                    <h1>Resource Not Found</h1>
                    <p>There is no URL associated with {url_resource}</p>
                </body>
            </html> 
            '''

PAGE_NO_URL = """
                <!DOCTYPE html>
                <html lang="en">
                <p>Please, enter a URL you would like to shorten</p>
                <form action="/" method="post">
                        URL to shorten: <input type="text" name="url">
                        <input type="submit" value="Submit">
                    </form> 
                    <body>
                        <p>Shortened URLS History: </p>
                        {url_dict}
                    </body>
                </html>
                """

class randomshortApp(webapp.webApp):

    def __init__(self, hostname, port):
        self.myShelveDict = shelve.open('DataBase')
        self.url_dict = self.myShelveDict['url_dict']
        super().__init__(hostname, port)


    def parse(self, request):
        method = request.split(' ', 1)[0]
        resource = request.split(' ', 2)[1]
        body_start = request.find('\r\n\r\n')

        if body_start == -1:
            body = None
        else:
            body = request[body_start + 4:]
        return method, resource, body


    def process(self, reqData):

        (method, resource, body) = reqData
        print(reqData)

        if method == 'GET':
            httpCode, htmlBody = self.get(resource)
        elif method == 'POST':
            httpCode, htmlBody = self.post(body)

        return httpCode, "<html><body>" + htmlBody + "</body></html>"


    def get(self, resource):
        if resource == '/':
            htmlBody = PAGE_URL_DICT.format(url_dict=self.url_dict)
            httpCode = "200 OK"
        else:
            url_resource = 'http://localhost:1234' + resource

            for key, value in self.url_dict.items():
                if url_resource == value:
                    htmlBody = REDIRECT.format(key=key)
                    httpCode = "302 REDIRECT"
                    break

            if url_resource not in self.url_dict.values():
                htmlBody = PAGE_RESOURCE_NOT_FOUND.format(url_resource=url_resource)
                httpCode = "404 Not Found"


        return httpCode, htmlBody


    def post(self, body):
        params = body.split('&')
        if params == ['']:
            htmlBody = PAGE_NO_URL.format(url_dict=self.url_dict)
            httpCode = "200 OK"
        else:
            for param in params:
                (name, value) = param.split("=", 1)
                if name == 'url':
                    if value != '':
                        url = self.urlPrefix(urllib.parse.unquote(value))
                        if url not in self.url_dict:
                            url_shorten = self.shortenURL()
                            self.url_dict[url] = url_shorten

                        htmlBody = PAGE_URL_DICT.format(url_dict=self.url_dict)
                        httpCode = "200 OK"
                        self.myShelveDict['url_dict'] = self.url_dict

                    else:
                        htmlBody = PAGE_NO_URL.format(url_dict=self.url_dict)
                        httpCode = "200 OK"

        return httpCode, htmlBody


    def urlPrefix(self, url):
        if not url.startswith('https://') and not url.startswith('http://'):
            prefix = "https://"
            url = "".join([prefix, url])

        return url


    def shortenURL(self):
        rand_resource = str(random.randint(0, 10000))
        url_shorten = "http://localhost:" + str(myPort) + "/" + rand_resource

        return url_shorten


if __name__ == "__main__":
    myPort = 1234
    testWebApp = randomshortApp("localhost", myPort)
